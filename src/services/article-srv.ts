/**
 * @description Servicio de Articulos
 */

import axios, { AxiosResponse } from 'axios';

// Models
import { Article } from '../models/Article';

/**
 * @description Consulta la api e inserta los datos
 * @returns void
 */
const taskInsertArticle = () => {
  return new Promise<void>(async (resolve) => {
    try {
      const { data }: AxiosResponse = await axios.get(env.URL_API);
      const { hits } = data;

      await Article.insertMany(hits, { ordered: false });

      console.log(`TOTAL HITS INSERTADOS --> ${hits.length}`);

      return resolve();

      // eslint-disable-next-line
    } catch (error: any) {
      if (error && error.result) {
        const { result } = error.result;
        console.log(`TOTAL HITS INSERTADOS --> ${result.nInserted}`);
      }
      return resolve();
    }
  });
};

/**
 * @description Consulta un articulo por el objectID
 * @param {string} objectID
 * @returns Article
 */
const getArticleById = (objectID: string) => {
  return new Promise(async (resolve, reject) => {
    try {
      const article = await Article.findOne({ objectID, status: true }).lean();

      if (!article) return resolve(`El articulo no existe o fue inhabilitado`);

      return resolve(article);
    } catch (error) {
      return reject(error);
    }
  });
};

/**
 * @description Consulta articulos mediante filtros
 * @param title
 * @param author
 * @param tags
 * @param paginate
 * @returns Articles
 */
const listArticlesByFilters = (
  title: string,
  author: string,
  tags: string[],
  // eslint-disable-next-line
  paginate: any
) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { page, limit } = paginate;

      // Añadimos la logica del skip de mongo
      const skip: number = page * limit - limit;

      interface Filter {
        $or?: object[];
        _tags?: object;
        title?: string;
        story_title?: string;
        author?: RegExp;
        status?: boolean;
      }

      const __filters: Filter[] = [{ status: true }];

      if (title) {
        const rgxTitle = new RegExp(title, 'ig');
        __filters.push({
          $or: [{ title: rgxTitle }, { story_title: rgxTitle }]
        });
      }

      if (author) {
        const rgxAuthor = new RegExp(author, 'ig');
        __filters.push({ author: rgxAuthor });
      }
      if (tags) __filters.push({ _tags: { $in: tags } });

      const articles = await Article.find({ $and: __filters })
        .lean()
        .sort({ objectID: 1 })
        .skip(skip)
        .limit(limit);

      return resolve(articles);
    } catch (error) {
      return reject(error);
    }
  });
};

/**
 * @description Edita un articulo por objectID
 * @param {string} objectID
 * @param {object} changes
 * @returns Article
 */
const editArticleById = (objectID: string, changes: object = {}) => {
  return new Promise(async (resolve, reject) => {
    try {
      const article = await Article.findOneAndUpdate(
        { objectID },
        { $set: changes },
        { new: true }
      ).lean();

      return resolve(article);
    } catch (error) {
      return reject(error);
    }
  });
};

/**
 * @description Elimina un articulo, que en realidad es pasarlo a status false
 * @param {string} objectID
 * @returns void
 */
const deleteArticleById = (objectID: string) => {
  return new Promise<void>(async (resolve, reject) => {
    try {
      await Article.updateOne({ objectID }, { $set: { status: false } }).lean();

      return resolve();
    } catch (error) {
      return reject(error);
    }
  });
};

export {
  taskInsertArticle,
  getArticleById,
  listArticlesByFilters,
  editArticleById,
  deleteArticleById
};
