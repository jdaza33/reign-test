/**
 * global
 */

declare global {
  // eslint-disable-next-line
  var env: any;

  interface GlobalInterface {
    value: unknown;
  }

  type GlobalType = {
    value: unknown;
  };
}

export {};
