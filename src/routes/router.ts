/**
 * Rutas de express
 */

import express from 'express';

const router = express.Router();

// Controllers
import {
  getArticle,
  listArticles,
  editArticle,
  deleteArticle
} from '../controllers/article-ctrl';

// Routes
router.get('/article/:id', getArticle);
router.post('/article/list', listArticles);
router.put('/article/:id', editArticle);
router.delete('/article/:id', deleteArticle);

export { router };
