/**
 * @description Server
 */

// Modules
import express, { Express } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import paginate from 'express-paginate';

// Routes
import { router } from '../routes/router';

const startExpress = () => {
  return new Promise<Express>((resolve, reject) => {
    try {
      //APP
      const app = express();

      //Middlewares
      app.use(cors());
      app.use(helmet());
      app.use(express.json());
      app.use(paginate.middleware(5, 50));

      //Routes
      app.use('/', router);

      return resolve(app);
    } catch (error) {
      return reject(error);
    }
  });
};

export { startExpress };
