/**
 * @description Precarga del archivo .env
 */

//Modules
import * as dotenv from 'dotenv';
import path from 'path';
dotenv.config();

const startDotenv = () => {
  return new Promise<void>(async (resolve, reject) => {
    try {
      const { parsed: ENV } = dotenv.config({
        path: path.resolve(__dirname, '../../', `.env.${process.env.NODE_ENV}`)
      });

      //Check ENV
      for (const i in ENV) {
        if (ENV[i] == undefined || ENV[i] == null || ENV[i] == '')
          return reject(`Variable ${i} is not defined or is empty`);
      }

      global.env = { ...ENV };
      return resolve();
    } catch (error) {
      return reject(error);
    }
  });
};

export { startDotenv };
