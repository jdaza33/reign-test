/**
 * @description Conexion a la base de datos de MongoDB
 */

import mongoose from 'mongoose';

const startMongoose = () => {
  return new Promise<void>((resolve, reject) => {
    try {
      mongoose.connect(env.URL_DATABASE, () => {
        console.log(`Base de datos conectada -->  ${env.URL_DATABASE}`);
        return resolve();
      });
    } catch (error) {
      return reject(error);
    }
  });
};

export { startMongoose };
