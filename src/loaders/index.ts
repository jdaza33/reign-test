/**
 * @description
 */

//Loaders
import { startDotenv } from './dotenv.loader';
import { startExpress } from './express.loader';
import { startMongoose } from './mongoose.loader';
import { cronService } from './cron.loader';

const init = () => {
  // eslint-disable-next-line
  return new Promise<any>(async (resolve, reject) => {
    try {
      await startDotenv();
      await startMongoose();
      await cronService();

      const app = await startExpress();

      return resolve(app);
    } catch (error) {
      return reject(error);
    }
  });
};

init()
  .then((app) => {
    app.listen(env.PORT, () => {
      console.log(`Servidor iniciado en el puerto ${env.PORT}`);
    });
  })
  .catch((err) => {
    console.log(err);
    throw new Error(err);
  });
