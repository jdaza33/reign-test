/**
 * @description Trabajos cron
 */

import cron from 'node-cron';

// Services
import { taskInsertArticle } from '../services/article-srv';

const cronService = () => {
  // Pasa cada hora
  cron.schedule('0 * * * *', () => {
    taskInsertArticle();
  });
};

export { cronService };
