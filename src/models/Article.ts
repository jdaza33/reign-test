/**
 * Esquema de mongoose de articulos
 */

import { model, Schema, Model, Document } from 'mongoose';

interface Article extends Document {
  title?: string;
  author: string;
  story_title?: string;
  _tags: string[];
  objectID: string;
  status?: boolean;
}

const ArticleSchema: Schema = new Schema({
  title: { type: String, required: false, index: true },
  author: { type: String, required: true, index: true },
  story_title: { type: String, required: false, index: true },
  _tags: { type: [String], required: false },
  objectID: { type: String, required: true, unique: true },
  status: { type: Boolean, required: true, default: true }
});

const Article: Model<Article> = model('Article', ArticleSchema);

export { Article };
