/**
 * @description Controlador de articulos
 */

import { Request, Response, NextFunction } from 'express';

// Services
import {
  getArticleById,
  listArticlesByFilters,
  editArticleById,
  deleteArticleById
} from '../services/article-srv';

const getArticle = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const article = await getArticleById(id);
    res.json({ success: 1, article });
  } catch (error) {
    next(error);
  }
};

const listArticles = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { title, tags, author } = req.body;
    const articles = await listArticlesByFilters(
      title,
      author,
      tags,
      req.query
    );
    res.json({ success: 1, articles });
  } catch (error) {
    next(error);
  }
};

const editArticle = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params;
    const filters = req.body;
    const article = await editArticleById(id, filters);
    res.json({ success: 1, article });
  } catch (error) {
    next(error);
  }
};

const deleteArticle = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { id } = req.params;
    await deleteArticleById(id);
    res.json({ success: 1, article: null });
  } catch (error) {
    next(error);
  }
};

export { getArticle, listArticles, editArticle, deleteArticle };
