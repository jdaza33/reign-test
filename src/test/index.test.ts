/**
 * @description Pruebas
 */

import { startExpress } from '../loaders/express.loader';
import chai, { expect } from 'chai';
import 'mocha';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);
chai.should();

// Test
describe('Test', () => {
  it('Solo es una prueba:)', function () {
    expect(true).to.equal(true);
  });
});

describe('POST /article/list', function () {
  it('Listar articulos', function (done) {
    startExpress().then((app) => {
      app.listen(3001);
      chai
        .request(app)
        .post('/article/list')
        .send({})
        .end(function (err, res) {
          if (err) console.log(err);
          expect(res).to.have.status(200);
        });
      done();
    });
  });
});
