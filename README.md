
# REST API - Reign TEST

REST API para una prueba de la empresa Reign creado por José Bolívar.
Tecnologias: Typescript, NodeJS, MongoDB y Mocha. 

### DEMO --> https://api-reign-test-jbolivar.herokuapp.com/
### POSTMAN --> https://documenter.getpostman.com/view/2941896/UVeCRU6s

## Install

    npm install

## Run the app

    npm run serve --> running on port 3001 by default

## Run the tests

    npm run test

# REST API

## Listar articulos

### Request

`POST /article/list/`

    curl -i -H 'Accept: application/json' http://localhost:3001/article/list/
### Filters
Se puede enviar en el body los siguientes parametros:

    {
	    "author": string;
	    "tags": string[];
	    "title": string;
    }
### Paginate
Por defecto en el query se envia los parametros `page` y `limit`.
Ejemplo: Para obtener un maximo de 5 resultados en la primera pagina se envia

    http://localhost:3001/article/list?page=1&limit=5

### Response

    {"success":1,"articles": []}

## Obtener un articulo dado el objectID

### Request

`GET /article/:id`

    curl -i -H 'Accept: application/json' http://localhost:3001/article/:id

### Response

    {"success":1,"article": {}}


## Editar un articulo

### Request

`PUT /article/:id`

    curl -i -H 'Accept: application/json' -d '{title: "Prueba"}' -X PUT http://localhost:3001/article/:id

### Response

    {"success":1,"article": {}}

## Eliminar un articuo

### Request

`DELETE /article/:id`

    curl -i -H 'Accept: application/json' -X DELETE http://localhost:3001/article/:id/

### Response

    {"success":1,"article": null}
